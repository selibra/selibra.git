## 更新日志

### v1.3.15
* 修复数据库连接池的BUG
* DI::getObjectContext()注释修改为泛型注释（class-string<T>）

### v1.3.14
* 修复路由BUG
* 增加`DollarPhrase`$字符映射处理
* 修复事务BUG

### v1.3.11
* 修复事务回退的BUG
* Redis增加`incr``incrBy`方法

### v1.3.10
* 增加`HttpServer` `debug`配置，开启Debug后，接口发生严重错误时将直接输出错误信息
* `Model` 增加`HttpServer`请求异常事件监听，将主动触发事务回退

### v1.3.9
* 修改`Aspect`注解，增加`AspectCallbackContext`，在调用`Aspect`时传入`AspectCallbackContext`参数，获取上下文

### v1.3.8
* 更新 `Dockerfile` 基础镜像为 `php:8.0.7-cli`，`swoole` 版本为`v4.6.7`
* 修改`HttpStartCommand->execute` 方法为 `protected` 

### v1.3.7
* 优化 启动步骤流程命令行提示
* 修改 数据库连接池和Redis连接池为Swoole连接池方式
* 增加 logNF打印方法，不打印文件
* 修改 dockerfile中swoole的版本
* 增加 `HttpOnManager`/`HttpOnStart`/`HttpOnWorkerStart`事件
* 增加 `Server::$server`/`Server::$input`/`Server::$output`静态属性，用于命令行操作，参考 [Symfony Console](https://symfony.com/doc/current/console.html)

### v1.3.6
* `Model/EntityProxy->toPropertyArrayData`增加数组判断，在读取Entity进行数据库存储时，对数组数据自动转换为JSON数据<br>
* 完善`InterfaceMethodTrigger`，增加`InterFaceMethodTriggerEvent`事件<br>
* 增加`LoggingEvent`事件，在通过`Log::record`记录日志时触发该事件<br>
* RedisClient->set增加`$expireTime`参数(过期时间)，支持传入具体时间(string，例如：2021-04-17 00:00:00 和 1618588800) 和 过期秒数(int，例如：3600（一个小时后过期）)
