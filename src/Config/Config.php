<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Config;


use Selibra\Di\DollarPhraseMapper\DollarPhrase;

class Config
{


    /**
     * 获取配置
     * @param string $configKey
     * @param mixed|null $default
     * @return mixed
     */
    public static function get(string $configKey, mixed $default = null): mixed
    {
        $value = Yaml::getIns()->get($configKey);
        if (is_array($value)) {
            $value = self::recursionResolve($value);
        } else {
            $value = DollarPhrase::resolve($value);
        }
        return $value ?? $default;
    }


    /**
     * @param $array
     * @return array
     */
    private static function recursionResolve($array): array
    {
        $result = [];
        foreach ($array as $key => $item) {
            if (is_array($item)) {
                $value = self::recursionResolve($item);
            } else {
                $value = DollarPhrase::resolve($item);
            }
            $result[$key] = $value;
        }
        return $result;
    }


    /**
     * 查看配置是否存在
     * @param string $configKey
     * @return mixed
     */
    public static function has(string $configKey)
    {
        return Yaml::getIns()->has($configKey);
    }

}
