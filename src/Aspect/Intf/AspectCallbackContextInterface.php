<?php
/*
 * @author eBIZ Team <dev@jungo.com.cn>
 * @copyright  Copyright (c) , ShenZhen JunGO Technology Co., Ltd. All rights reserved.
 * @license  Commercial authorization, unauthorized use and modification are prohibited
 * @url www.jungo.com.cn
 */
namespace Selibra\Aspect\Intf;

interface AspectCallbackContextInterface
{

    /**
     * 获取方法的参数
     * @var array|null
     */
    public function getFunctionParams(): ?array;

    /**
     * 获取方法的返回值
     * @var mixed
     */
    public function getFunctionReturnValue(): mixed;

    /**
     * @return \Throwable|null
     */
    public function getFunctionThrow(): ?\Throwable;

}
