<?php
/**
 * Copyright (c) [2021] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */
namespace Selibra\Aspect\Events;

class InterfaceMethodTriggerEvent
{

    protected array $arguments;

    protected string $namespace;

    protected string $method;

    /**
     * 结果
     * @var
     */
    protected $result;


    public function __construct($arguments,$namespace,$method)
    {
        $this->arguments = $arguments;
        $this->namespace = $namespace;
        $this->method = $method;
    }

    /**
     * @return array
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return InterfaceMethodTriggerEvent
     */
    public function setArguments(array $arguments): InterfaceMethodTriggerEvent
    {
        $this->arguments = $arguments;
        return $this;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     * @return InterfaceMethodTriggerEvent
     */
    public function setNamespace(string $namespace): InterfaceMethodTriggerEvent
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return InterfaceMethodTriggerEvent
     */
    public function setMethod(string $method): InterfaceMethodTriggerEvent
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     * @return InterfaceMethodTriggerEvent
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }


}
