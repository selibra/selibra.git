<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Aspect\Annotations;


use Selibra\Aspect\AdviceCollector;
use Selibra\Aspect\AspectConstants;
use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use \Attribute;

/**
 * Class Before
 * @package selibra\aspect\annotations
 */
#[Attribute(Attribute::TARGET_METHOD)]
final class Before implements SelibraAnnotationInterface
{

    private string $namespace;

    private string $action;

    /**
     * Before constructor.
     * @param $namespace
     * @param $action
     */
    public function __construct($namespace,$action)
    {
        $this->namespace = $namespace;
        $this->action = $action;
    }

    /**
     * @inheritDoc
     */
    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {
        AdviceCollector::register($this->namespace, $this->action, $annotationExecEntity->getClass(), $annotationExecEntity->getMethod()->getName(), AspectConstants::TYPE_BEFORE);
    }

    /**
     * @inheritDoc
     */
    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }
}