<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di\Annotations;

use \Attribute;
use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use Selibra\Di\DollarPhraseMapper\DollarPhraseMapperCollect;

#[Attribute(Attribute::TARGET_CLASS)]
final class DollarPhraseMapper implements SelibraAnnotationInterface
{


    public function __construct(protected string $character)
    {
    }

    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {
        DollarPhraseMapperCollect::addMapper($this->character, $annotationExecEntity->getClass());
    }

    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }


}
