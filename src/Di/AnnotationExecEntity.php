<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di;


use Selibra\Di\Annotations\SelibraAnnotationConstants;

final class AnnotationExecEntity
{

    /**
     * @var string
     */
    private string $execTime;


    /**
     * @var \ReflectionProperty
     */
    private \ReflectionProperty $property;


    /**
     * @var object
     */
    private object $object;


    /**
     * @var \ReflectionMethod
     */
    private \ReflectionMethod $method;


    /**
     * @var Metadata
     */
    protected Metadata $metadata;


    public function __construct(string $execTime = SelibraAnnotationConstants::EXEC_INIT)
    {
        $this->execTime = $execTime;
    }

    /**
     * @return \ReflectionProperty
     */
    public function getProperty(): \ReflectionProperty
    {
        return $this->property;
    }

    /**
     * @param \ReflectionProperty $property
     */
    public function setProperty(\ReflectionProperty $property): void
    {
        $this->property = $property;
        unset($property);
    }


    /**
     * @return object
     */
    public function getObject(): object
    {
        return $this->object;
    }

    /**
     * @param object $object
     */
    public function setObject(object $object): void
    {
        $this->object = $object;
    }

    /**
     * @return string
     */
    public function getExecTime(): string
    {
        return $this->execTime;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->getMetadata()->getNamespace();
    }


    /**
     * @return \ReflectionMethod
     */
    public function getMethod(): \ReflectionMethod
    {
        return $this->method;
    }

    /**
     * @param \ReflectionMethod $method
     */
    public function setMethod(\ReflectionMethod $method): void
    {
        $this->method = $method;
    }

    /**
     * 是否存在Method
     * @return bool
     */
    public function hasMethod(): bool
    {
        if (isset($this->method)) {
            return true;
        }
        return false;
    }

    /**
     * @return Metadata
     */
    public function getMetadata(): Metadata
    {
        return $this->metadata;
    }

    /**
     * @param Metadata $metadata
     */
    public function setMetadata(Metadata $metadata): void
    {
        $this->metadata = $metadata;
    }

}