<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di;


use LeoMQ\Annotations\ConsumerRealize;
use Selibra\Di\Annotations\Component;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use Selibra\Tools\Console;

class ContainerObjectInitialization
{

    protected Metadata $metadata;


    public function __construct($metadata)
    {
        $this->metadata = $metadata;
    }


    /**
     * @throws \ReflectionException
     */
    public function run()
    {
        $annotationExecEntity = new AnnotationExecEntity();
        $annotationExecEntity->setMetadata($this->metadata);
        $classAnnotations = $this->metadata->getClassAnnotations();
        foreach ($classAnnotations as $annotation) {
            if ($annotation instanceof Component && empty($componentAnnotation)) {
                // 最后执行 Component
                continue;
            } elseif ($annotation instanceof SelibraAnnotationInterface) {
                $annotation->exec($annotationExecEntity);
            }
        }
    }


    /**
     * @throws \ReflectionException
     */
    public function runComponentAnnotation()
    {
        $annotationExecEntity = new AnnotationExecEntity();
        $annotationExecEntity->setMetadata($this->metadata);
        $classAnnotations = $this->metadata->getClassAnnotations();
        foreach ($classAnnotations as $annotation) {
            if ($annotation instanceof Component && empty($componentAnnotation)) {
                // 最后执行 Component
                $annotation->exec($annotationExecEntity);
            }
        }
    }


    /**
     * @param Metadata $metadata
     * @throws \ReflectionException
     */
    public static function methodsAnnotationExec(Metadata $metadata)
    {
        $annotationExecEntity = new AnnotationExecEntity();
        $annotationExecEntity->setMetadata($metadata);
        $methodsAnnotations = $metadata->getMethodsAnnotations();
        foreach ($methodsAnnotations as $methodName => $methodAnnotations) {
            $method = $metadata->getReflectionClass()->getMethod($methodName);
            foreach ($methodAnnotations as $methodAnnotation) {
                if ($methodAnnotation instanceof SelibraAnnotationInterface) {
                    $annotationExecEntity->setMethod($method);
                    $methodAnnotation->exec($annotationExecEntity);
                }
            }
        }
    }


    /**
     * @param string $component
     * @throws \ReflectionException
     */
    public static function componentMethodRun(string $component)
    {
        $classes = DI::getAnnotationClasses($component);
        foreach ($classes as $class) {
            $metadata = DI::getContainer()->collector()->get($class);
            self::methodsAnnotationExec($metadata);
        }
    }


}
