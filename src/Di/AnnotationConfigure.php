<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di;


use Selibra\Di\Annotations\Component;

class AnnotationConfigure
{

    /**
     * @var string
     */
    protected string $relyOnComponent;


    /**
     * 设置依赖组件
     * @param $componentClass
     * @return $this
     */
    public function setRelyOnComponent($componentClass)
    {
        $this->relyOnComponent = $componentClass;
        return $this;
    }

    /**
     * 获取依赖组件
     * @return string
     */
    public function getRelyOnComponent(): ?string
    {
        if( !isset($this->relyOnComponent) ){
            return null;
        }
        return $this->relyOnComponent;
    }

}