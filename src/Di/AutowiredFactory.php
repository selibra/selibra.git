<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di;


use Selibra\Tools\Tools;

class AutowiredFactory
{

    /**
     * @var object[]
     */
    private static array $instantiations = [];


    /**
     * @param $name
     * @return object
     */
    public static function getInstantiation($name, $coroutine = true)
    {
        $key = 'PROCESS';
        if ($coroutine && Tools::isCoroutine()) {
            $key = Tools::getCid();
            Tools::coroutineDefer(function () use ($key) {
                unset(self::$instantiations[$key]);
            });
        }
        if (empty(self::$instantiations[$key][$name])) {
            self::$instantiations[$key][$name] = DI::getObjectContext($name);
        }
        return self::$instantiations[$key][$name];
    }

}
