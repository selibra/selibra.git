<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Di\Container;


use Selibra\Di\DI;
use Selibra\Di\Metadata;


/**
 * Class ContainerCollector
 * 容器内容收集器
 * @package selibra\di\container
 */
class ContainerCollector
{

    /**
     * @var array<Metadata>
     */
    protected array $collectors = [];


    /**
     * @var array<string>
     */
    protected array $collectorsGroupIndex = [];


    /**
     * 接口索引
     * @var array
     */
    protected array $collectorsInterfaceIndex = [];


    /**
     * @var Container
     */
    protected static Container $container;


    /**
     * 添加到收集器中
     * @param string $namespace
     * @param Metadata $metadata
     * @return null
     */
    public function add(string $namespace, Metadata $metadata)
    {
        // 注册到基础容器
        $this->collectors[$metadata->getProxyClassName()] = $metadata;

        // 注册命名空间到索引
        $annotations = $metadata->getClassAnnotations();
        foreach ($annotations as $annotation) {
            $annotation = get_class($annotation);
//            if ($annotation === Target::class) {
//                continue;
//            }
            $this->collectorsGroupIndex[$annotation] ??= [];
            $this->collectorsGroupIndex[$annotation][$namespace] = $namespace;
        }

        // 注解到接口存储器
        $interfaces = $metadata->getReflectionClass()->getInterfaceNames();
        foreach ($interfaces as $interface) {
            $this->collectorsInterfaceIndex[$interface][] = $namespace;
        }
    }


    /**
     * @param $namespace
     * @return Metadata
     */
    public function get(string $namespace)
    {
        $namespace = DI::getClassProxyClassName($namespace);
        if (empty($this->collectors[$namespace])) {
            return null;
        }
        return $this->collectors[$namespace];
    }


    /**
     * @param $namespace
     * @return Metadata
     */
    public function getInterfaceImplementClasses(string $namespace)
    {
        if( !array_key_exists($namespace,$this->collectorsInterfaceIndex) ){
            return null;
        }
        return $this->collectorsInterfaceIndex[$namespace];
    }


    /**
     * @param string $containerGroupName
     * @return array<string>
     */
    public function selectObjectByContainerGroup(string $containerGroupName): array
    {
        if( !array_key_exists($containerGroupName,$this->collectorsGroupIndex) ){
            return [];
        }
        return $this->collectorsGroupIndex[$containerGroupName];
    }


    /**
     * @param Container $container
     */
    public static function setContainer(Container $container)
    {
        self::$container = $container;
    }


    /**
     * @return Container
     */
    public static function getContainer()
    {
        return DI::getContainer();
    }


}