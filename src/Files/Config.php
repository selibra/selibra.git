<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Files;

use Selibra\Config\Config as SelibraConfig;

class Config
{

    /**
     * @var string
     */
    protected static string $rootPath;


    /**
     * @return string
     */
    public static function getRootPath()
    {
        if (empty(self::$rootPath)) {
            self::$rootPath = PHP_SAPI === 'cli' ? dirname($_SERVER['PHP_SELF']) : './';
            if (self::$rootPath === '$') {
                self::$rootPath = realpath('./');
            }
        }
        return self::$rootPath;
    }

}