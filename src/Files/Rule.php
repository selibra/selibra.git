<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Files;


use Selibra\Files\Protocol\RuleInterface;

class Rule implements RuleInterface
{

    /**
     * @var string
     */
    protected $prefix;


    /**
     * @var string
     */
    protected $suffix;


    /**
     * @var string
     */
    protected $path;


    /**
     * @var Ignore
     */
    protected Ignore $ignore;

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     * @return $this
     */
    public function setPrefix(string $prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * @param string $suffix
     * @return $this
     */
    public function setSuffix(string $suffix)
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return Ignore
     */
    public function getIgnore(): Ignore
    {
        if( !isset($this->ignore) ){
            $this->ignore = new Ignore();
        }
        return $this->ignore;
    }

    /**
     * @param Ignore $ignore
     */
    public function setIgnore(Ignore $ignore): void
    {
        $this->ignore = $ignore;
    }

}