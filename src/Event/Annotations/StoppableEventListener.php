<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Event\Annotations;

use Selibra\Di\AnnotationConfigure;
use Selibra\Di\AnnotationExecEntity;
use Selibra\Di\Annotations\Protocol\SelibraAnnotationInterface;
use \Attribute;

/**
 * Class StoppableEventListener
 * @package Selibra\Event\Annotations
 */
#[Attribute]
class StoppableEventListener implements SelibraAnnotationInterface
{

    /**
     * @inheritDoc
     */
    public function exec(AnnotationExecEntity &$annotationExecEntity)
    {

    }

    /**
     * @inheritDoc
     */
    public function configure(AnnotationConfigure $annotationConfigure)
    {
        // TODO: Implement configure() method.
    }
}