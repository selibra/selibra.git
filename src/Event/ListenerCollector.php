<?php
/**
 * Copyright (c) [2019] [吴跃忠]
 * [selibra] is licensed under the Mulan PSL v1.
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 * http://license.coscl.org.cn/MulanPSL
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR
 * PURPOSE.
 * See the Mulan PSL v1 for more details.
 */

namespace Selibra\Event;


use Selibra\Tools\Console;

class ListenerCollector
{

    /**
     * @var array
     */
    protected static array $listeners = [];

    /**
     * @param string $listerClass
     * @param string $listerMethod
     * @param string $eventClass
     */
    public static function register(string $eventClass, string $listerClass, string $listerMethod = '', string $type = Listener::LISTENER_TYPE_CLASS)
    {
        self::$listeners[$eventClass][] = [
            'class' => $listerClass,
            'method' => $listerMethod,
            'type' => $type
        ];
    }


    /**
     * 或者事件监听者
     * @param string $eventClass
     * @return array|null
     */
    public static function getListeners(string $eventClass): array|null
    {
        if (array_key_exists($eventClass, self::$listeners)) {
            return self::$listeners[$eventClass];
        }
        return [];
    }

}