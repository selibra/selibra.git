FROM php:8.0.7-cli

ENV TIME_ZONE=Asia/Shanghai

# 换源、安装基础软件
RUN sed -i "s/archive.ubuntu./mirrors.aliyun./g" /etc/apt/sources.list \
    && sed -i "s/deb.debian.org/mirrors.aliyun.com/g" /etc/apt/sources.list \
    && sed -i "s/httpredir.debian.org/mirrors.aliyun.com\/debian-security/g" /etc/apt/sources.list \
    && apt-get clean \
    && apt-get update \
    && apt-get install -y openssl \
    && apt-get install -y libssl-dev \
    && apt-get install -y libwebp-dev libjpeg-dev libpng-dev libfreetype6-dev zip unzip cmake wget git \
    && ln -snf /usr/share/zoneinfo/$TIME_ZONE /etc/localtime && echo $TIME_ZONE > /etc/timezone \

    # 安装composer
    && curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && composer config -g repo.packagist composer https://packagist.phpcomposer.com

# 安装swoole
RUN git clone -b v4.6.7 https://gitee.com/swoole/swoole.git swoole \
    && cd swoole \
    && phpize \
    && ./configure --enable-openssl \
    && make \
    && make install \
    && curl https://codeload.github.com/redis/hiredis/zip/master --output hiredis.zip \
    && unzip hiredis.zip \
    && cd hiredis-master \
    && make \
    && make install \
    && wget http://download.savannah.gnu.org/releases/freetype/freetype-2.10.0.tar.gz \
    && tar zxvf freetype-2.10.0.tar.gz \
    && cd freetype-2.10.0 \
    && ./configure --prefix=/usr/local/freetype \
    && make && make install \
    && docker-php-source extract \
    && cd /usr/src/php/ext/gd \
    && docker-php-ext-configure gd --with-freetype=/usr/local/freetype \
    && docker-php-ext-install gd \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install pdo pdo_mysql



RUN git clone https://github.com/phpredis/phpredis.git phpredis \
    && cd phpredis \
    && phpize \
    && ./configure \
    && make \
    && make install \
    && apt-get install -y net-tools \
    && apt-get install -y libyaml-dev \
    && wget https://pecl.php.net/get/yaml-2.2.0b2.tgz \
    && tar zxvf yaml-2.2.0b2.tgz \
    && cd yaml-2.2.0b2 && phpize && ./configure && make && make install && make clean

RUN cd /usr/local/etc/php/ \
    && echo 'extension=/usr/local/lib/php/extensions/no-debug-non-zts-20200930/swoole.so' >> php.ini \
    && echo 'extension=/usr/local/lib/php/extensions/no-debug-non-zts-20200930/redis.so' >> php.ini \
    && echo 'extension=/usr/local/lib/php/extensions/no-debug-non-zts-20200930/yaml.so' >> php.ini
